import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Route } from '@angular/router';
import { PageNotFoundComponent } from '@akkor-hotel/shared/ui';
import { appRoutes } from './app.routes';

describe('AppRoutingModule', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(appRoutes)],
      declarations: [PageNotFoundComponent],
    });
  });

  it('should have a route for empty path', () => {
    const emptyPathRoute = appRoutes.find((route: Route) => route.path === '');
    expect(emptyPathRoute).toBeTruthy();
  });


  it('should have a wildcard route for PageNotFoundComponent', () => {
    const wildcardRoute = appRoutes.find((route: Route) => route.path === '**');
    expect(wildcardRoute).toBeDefined();
    if (wildcardRoute) {
      expect(wildcardRoute).toBeTruthy();
      expect(wildcardRoute.component).toBe(PageNotFoundComponent);
    }
  });
});
